import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './pages/not-found/not-found.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { MainLayoutComponent } from './layouts/main-layout/main-layout.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { RouterModule } from '@angular/router';
import { MenubarModule} from 'primeng/menubar';
import { ButtonModule } from 'primeng/button';
import { InputTextModule}  from 'primeng/inputtext';
import {TabMenuModule} from 'primeng/tabmenu';
import { MatToolbarModule, MatSidenavModule, MatListModule, MatButtonModule, MatIconModule } from "@angular/material";
import { FlexLayoutModule } from '@angular/flex-layout';

 
@NgModule({
  declarations: [NotFoundComponent, TopbarComponent, MainLayoutComponent, HomePageComponent],
  imports: [
    CommonModule,
    RouterModule,
    MenubarModule,
    ButtonModule,
    InputTextModule,
    TabMenuModule, 
    MatSidenavModule,
    MatIconModule, 
    MatButtonModule,
    MatToolbarModule,
    MatListModule, 
    FlexLayoutModule
  ]
})
export class CoreModule { }
