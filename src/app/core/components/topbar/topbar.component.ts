import { MenuItem} from 'primeng/api';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {
  items: MenuItem[];
  title = 'app';
  constructor() { }

  ngOnInit(): void {
    this.items = [
      {label: 'Form', icon: 'pi pi-fw pi-file', routerLink:  ['/passenger-form']},
      {label: 'Home', icon: 'pi pi-fw pi-home', routerLink:  ['/']}
  ];
  }


}
