import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-root',
  template: '<router-outlet> </router-outlet>',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'plantilla-rest';
}
