export class Passenger{
    public documentNumber: string;
    public documentCountry: string;
    public documentType: string;
}