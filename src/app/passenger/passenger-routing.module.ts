import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PassengerFormComponent } from './pages/passenger-form/passenger-form.component';


const routes: Routes = [
  {path: '', component: PassengerFormComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PassengerRoutingModule { }
