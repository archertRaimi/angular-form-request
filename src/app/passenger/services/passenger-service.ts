import { Injectable } from '@angular/core';
import { Passenger } from '../dto/Passenger';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { first, map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { EnrrolledResponse } from '../dto/enrolledResponse';



@Injectable({
  providedIn: 'root'
})
export class PassengerService {

  public passenger: Passenger = new Passenger();

  constructor(private http:HttpClient) { }

  public sendPassengerPost(passenger:Passenger, company:String): Promise<String> {
   
    console.log("LLEGA AL SERVICE")
    let httpHeaders: HttpHeaders = new HttpHeaders({
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods' : 'POST',
      //'Access-Control-Allow-Headers' : 'Content-Type, Authorization',
      'company' : 'xxxx',
      'Content-Type' : 'application/json'
    });

    let options = {
      headers: httpHeaders
    }

    const body: URLSearchParams = new URLSearchParams();
    body.append('documentNumber', passenger.documentNumber);
    body.append('documentCountry', passenger.documentCountry);
    body.append('documentType', passenger.documentType);

    this.http.post<any>('http://localhost:8080/dummy', passenger, options).subscribe(data => {
      console.log("entra peticion");
    })

    return null;
  }

  public sendPassengerGet(passenger:Passenger, company:string):Promise<EnrrolledResponse> {
   
    console.log("LLEGA AL SERVICE")
    let httpHeaders: HttpHeaders = new HttpHeaders({
      'Access-Control-Allow-Origin' : '*',
      'Access-Control-Allow-Methods' : 'GET',
      'Content-Type' : 'application/json',
      'company' : company
    });

    let httpParams = new HttpParams().set('documentNumber',passenger.documentNumber).set('documentType', passenger.documentType).set('documentCountry', passenger.documentCountry);

    const options = {
      params: httpParams,
      headers: httpHeaders
    }

    return this.http.get<EnrrolledResponse>('http://localhost:8080/dummy2', options).toPromise();

  }

}
