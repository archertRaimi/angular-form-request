import { Component, OnInit, Input } from '@angular/core';
import { Passenger } from '../../dto/Passenger';
import { Message } from 'primeng/api';
import { PassengerService } from '../../services/passenger-service';
import { EnrrolledResponse } from '../../dto/enrolledResponse';


@Component({
  selector: 'app-passenger-form',
  templateUrl: './passenger-form.component.html',
  styleUrls: ['./passenger-form.component.css']
})
export class PassengerFormComponent implements OnInit {


  @Input() public passenger:Passenger = new Passenger();
  @Input() public company:string;
  public msgs: Message[] = [];
  public boardingCard:string;
  public enrolledResponse: EnrrolledResponse = new EnrrolledResponse();

  constructor(private passengerService: PassengerService) { }

  ngOnInit(): void {
    this.passenger.documentNumber = '';
    this.passenger.documentType = '';
    this.passenger.documentCountry = '';
  }

  sendPassengerEnter(evt): void {
    if (evt.key === 'Enter') {
      this.sendPassenger();
    }
  }

  public async sendPassenger(){
    console.log(this.passenger.documentNumber);
    console.log(this.passenger.documentCountry);
    console.log(this.passenger.documentType);
    console.log(this.company);
    this.enrolledResponse = await this.passengerService.sendPassengerGet(this.passenger, this.company);

    if(this.enrolledResponse.enrrolled){
      this.msgs = [];
      this.msgs.push({severity:'success', summary:'Success Message', detail:'Person Enrolled'});
    }else{
      this.msgs = [];
      this.msgs.push({severity:'warn', summary:'Warn Message', detail:'Person Not Enrolled'});
    }
  }

}
