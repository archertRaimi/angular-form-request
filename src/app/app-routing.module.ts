import { NgModule, Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './core/pages/not-found/not-found.component';
import { MainLayoutComponent } from './core/layouts/main-layout/main-layout.component';
import { HomePageComponent } from './core/pages/home-page/home-page.component';

const routes: Routes = [
/*
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    component: MainLayoutComponent
  }*/
  {
    path: '',
    component: MainLayoutComponent,
    children: [ 
      {path: '', component: HomePageComponent},
      {path: 'passenger-form', loadChildren:() => import('./passenger/passenger.module').then(m => m.PassengerModule)}
    ]
  },
  { path: '**', component: NotFoundComponent}

  /*
  {//declaras el nombre y su compone
    path: 'not-found',
    component: NotFoundComponent
  },
  {//le asignas la ruta
    path: '**',
    redirectTo: '/not-found'
  }*/
]

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
